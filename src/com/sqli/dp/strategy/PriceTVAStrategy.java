package com.sqli.dp.strategy;

public class PriceTVAStrategy implements Strategy {
    //    tva attribute
    private final int tva=5;



    @Override
    public int calculerCout(int prixProduit, int quantite) {
        int prix= prixProduit +prixProduit*tva/100;
        return prix *quantite;
    }
}
