package com.sqli.dp.strategy;

public class PromotionStrategy implements Strategy {

    //Promotion attribute
    private final int promotion=30;


    @Override
    public int calculerCout(int prixProduit, int quantite) {
        int prix= prixProduit -prixProduit*promotion/100;
        return prix *quantite;
    }
}
