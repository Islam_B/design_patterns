package com.sqli.dp.strategy;

public interface Strategy {

    int calculerCout(int prixProduit, int quantite);

}
