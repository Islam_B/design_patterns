/**
 * 
 */
package com.sqli.dp.strategy;

/**
 * @author ehettifouri
 * 
 */
public class Panier {

    private int prixProduit;

    private int quantite;
    private Strategy strategy;

    public Panier(Strategy strategy) {
        this.strategy=strategy;
    }


    int calculerCout() {
    	return strategy.calculerCout(prixProduit,prixProduit);

    }

    /**
     * @return Attribut {@link #prixProduit}
     */
    public int getPrixProduit() {
        return prixProduit;
    }

    /**
     * @param pPrixProduit Valeur à affecter à l'attribut {@link #prixProduit}
     */
    public void setPrixProduit(final int pPrixProduit) {
        prixProduit = pPrixProduit;
    }

    /**
     * @return Attribut {@link #quantite}
     */
    public int getQuantite() {
        return quantite;
    }

    /**
     * @param pQuantite Valeur à affecter à l'attribut {@link #quantite}
     */
    public void setQuantite(final int pQuantite) {
        quantite = pQuantite;
    }

}
