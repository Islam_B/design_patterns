/**
 * 
 */
package com.sqli.dp.decorator;

/**
 * @author ehettifouri
 * 
 * j'ai un caffe et je veux ajouter Lait et Chocola 
 * 
 * Lait : caffe, eau Lait
 * Chocola : caffe eau lait chocola
 * 
 * en appliquant le DP decorator
 *  
 */
public class MainTest {

    public static void main(final String[] args) {
        /**
         * before we had just simple caffe then we addded the wrapper interface Coffe
         * that is implemented by our simplCoffe class and by our new decprator
         *
         */
        Coffee caffeNoir = new SimpleCaffe();
        displayMenu(caffeNoir);
        Coffee caffeAuLait = new Lait(new SimpleCaffe());
        displayMenu(caffeAuLait);

        Coffee caffeeChocolatAuLait = new CHocolla(new Lait(new SimpleCaffe()));
        displayMenu(caffeeChocolatAuLait);

    }

    /**
     * @param pCaffe
     */
    private static void displayMenu(final Coffee pCaffe) {

        System.out.println("Ingred : " + pCaffe.getIngeriend());
        System.out.println("avec Prix : " + pCaffe.calculeCout());
        System.out.println("-----------------");
    }
}
