package com.sqli.dp.decorator;

public class CHocolla extends CoffeDecorator {
    public CHocolla(Coffee coffe) {
        super(coffe);
    }

    @Override
    public int calculeCout() {
        return super.calculeCout()+3;
    }

    @Override
    public String getIngeriend() {
        return super.getIngeriend()+", chocolat";
    }
}
