package com.sqli.dp.decorator;

public class CoffeDecorator implements Coffee {
    private Coffee coffe;

    public CoffeDecorator(Coffee coffe) {
        this.coffe=coffe;
    }

    @Override
    public int calculeCout() {
        return coffe.calculeCout();
    }

    @Override
    public String getIngeriend() {
        return coffe.getIngeriend();
    }
}
