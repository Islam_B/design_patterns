package com.sqli.dp.visiteur;

public class LecteurVisiteur extends Visitor {
    @Override
    public void visite(PageDeGarde pageGarde) {
        System.out.println("Visiting the "+pageGarde.getContenu());
    }

    @Override
    public void visite(Sommair sommaire) {
        System.out.println("Visiting the "+sommaire.getContenu());
    }

    @Override
    public void visite(Chapitre chapitre) {
        System.out.println("Visiting the "+chapitre.getContenu());
    }
}
