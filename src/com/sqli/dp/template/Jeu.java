package com.sqli.dp.template;

public abstract class Jeu {

	abstract void louerTerrain();
	abstract void commencerAJouer();
	abstract void prendreUneDouche();


	/**
	 *
	 *This is the template method
	 */
	public final void jouer() {
		louerTerrain();
		commencerAJouer();
		prendreUneDouche();
	}
	
}
