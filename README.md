# Design patterns examples


## Desing patterns included

* **Structural desing patterns** 
    * Adapter
    * Decorator
    
    
* **Behavior desing patterns** 
    * Strategy
    * Visitor
    * Template
    